﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UwpLibrary
{
    public class Car
    {
        private string _vinNumber;
        private string _carMake;
        private string _type;
        private float _purchasePrice;
        private int _modelYear;
        private int _mileage;
        private float _deprictedValue;
       

        public override string ToString()
        {
            return $"{VinNumber},{CarMake},{Type},{PurchasePrice},{ModelYear},{Mileage},{DeprictedValue} ";
        }

        public Car(string vinNumber, string carMake, string type,
            float purchasePrice, int modelYear, int mileage,float deprictedValue)
        {
            this.VinNumber = vinNumber;
            this.CarMake = carMake;
            this.Type = type;
            this.PurchasePrice = purchasePrice;
            this.ModelYear = modelYear;
            this.Mileage = mileage;
            this.DeprictedValue = deprictedValue;
        }



        public string VinNumber
        {
            get
            {
                return _vinNumber;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Vin Number  cannot be blank");
                _vinNumber = value;
            }

        }

        public string CarMake
        {
            get
            {
                return _carMake;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Car Make  cannot be blank");
                _carMake = value;
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public float PurchasePrice
        {
            get
            {
                return _purchasePrice;
            }
            set
            {
                if (value < 0)
                    throw new Exception("Purchase Price cannot be negative");
                _purchasePrice = value;
            }
        }

        public int ModelYear
        {
            get
            {
                return _modelYear;

            }
            set
            {
                _modelYear = value;
            }
        }
        public int Mileage
        {
            get
            {
                return _mileage;
            }
            set
            {
                if (value < 0)
                    throw new Exception("Mileage must be positive");

                _mileage = value;
            }

        }
        public float DeprictedValue
        {
            get
            {
                return _deprictedValue;
            }
            set
            {
                _deprictedValue = value;
            }
        }
    }
}
