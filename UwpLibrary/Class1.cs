﻿using System;

namespace UwpLibrary
{
    public enum CarType //enum of Cars
    {
        Crossover,
        Sedan,
        Coupe,
        Hatchback,
        SUV,
        Convertible
    }
}
