﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UwpLibrary
{
    public class CarRepository
    {
        private List<Car> _cars = new List<Car>();    // maintaining the list of cars.

        //add Car
        public void AddCar(Car c)
        {

            if (GetByVin(c.VinNumber) != null)
            {
                throw new Exception("Car already exists.");
            }
            _cars.Add(c);
        }

        //search by Vin Number
        public Car GetByVin(string VinNumber)
        {
            foreach (Car c in _cars)
            {
                if (c.VinNumber == VinNumber)
                {
                    return c;
                }
            }
            return null;
        }
    }
}
