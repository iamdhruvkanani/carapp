﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using UwpLibrary;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WorkingWithUWP
{
    // An empty page that can be used on its own or navigated to within a Frame.
    
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            CmbTypeCar.ItemsSource = Enum.GetValues(typeof(CarType));//storing enum values in combo box
        }
        private CarRepository _carRepository = new CarRepository();
        



    private Car CaptureCarInformation() // method for capturing car information
        {
            string vinNumber = TxtVinNumber.Text;
            string carMake = TxtCarMake.Text;
            float purchasePrice = float.Parse(TxtPurchasePrice.Text);
            int modelYear = int.Parse(CmbModelYear.SelectedItem.ToString());
            int mileage = int.Parse(TxtMileage.Text);
            string carType =CmbTypeCar.SelectedItem.ToString() ;

            

            float deprictedValue = ComputedProperty();
            
            Car c = new Car(vinNumber, carMake, carType, purchasePrice, modelYear, mileage,deprictedValue); //passing all values to Car
            return c;

        }
       private float ComputedProperty() //method for calculating depricated price
        {
          
            float yearValue = 0;
            float mileValue = 0;
            int carYear = int.Parse(CmbModelYear.SelectedItem.ToString());
            int carMile = int.Parse(TxtMileage.Text);
            float carPrice = float.Parse(TxtPurchasePrice.Text);
            float deprictedValue = 0;
            
            switch (carYear)
            {
                case 2019:
                    yearValue = (carPrice * 10) / 100;
                    break;
                case 2018:
                    yearValue = (carPrice * 20) / 100;
                    break;
                case 2017:
                    yearValue = (carPrice * 30) / 100;
                    break;
                case 2016:
                    yearValue = (carPrice * 40) / 100;
                    break;
                case 2015:
                    yearValue = (carPrice * 50) / 100;
                    break;
                case 2014:
                    yearValue = (carPrice * 60) / 100;
                    break;
                case 2013:
                    yearValue = (carPrice * 70) / 100;
                    break;
                case 2012:
                    yearValue = (carPrice * 80) / 100;
                    break;
                case 2011:
                    yearValue = (carPrice * 90) / 100;
                    break;
                case 2010:
                    yearValue = (carPrice * 100) / 100;
                    break;
                
            }
            if(carMile >=  10000 && carMile <20000){
                mileValue = (carPrice * 9) / 100;
            }
            else if(carMile >= 20000 && carMile < 30000)
            {
                mileValue = (carPrice * 18 )/ 100;
            }
            else if (carMile >= 30000 && carMile < 40000)
            {
                mileValue = (carPrice * 27) / 100;
            }
            else if (carMile >= 40000 && carMile < 50000)
            {
                mileValue = (carPrice * 36 )/ 100;
            }
            else if (carMile >= 50000 && carMile < 60000)
            {
                mileValue = (carPrice * 45) / 100;
            }
            else if (carMile >= 60000 && carMile < 70000)
            {
                mileValue = (carPrice * 54) / 100;
            }
            else if (carMile >= 70000 && carMile < 80000)
            {
                mileValue = (carPrice * 63) / 100;
            }
            else if (carMile >= 80000 && carMile < 90000)
            {
                mileValue = (carPrice * 72) / 100;
            }
            else if (carMile >= 90000 && carMile < 100000)
            {
                mileValue = (carPrice * 81) / 100;
            }
            else if (carMile >= 100000 && carMile < 110000)
            {
                mileValue = (carPrice * 90) / 100;
            }
            else if(carMile >= 110000 && carMile < 120000)
            {
                mileValue = (carPrice * 99 )/ 100;
            }
            
            deprictedValue = carPrice - (yearValue + mileValue);

           
            return deprictedValue;
          

        }


        private void RenderCarInfo(Car car) // method for rendering car info
        {
            TxtVinNumber.Text = car.VinNumber;
            TxtCarMake.Text = car.CarMake;
            TxtPurchasePrice.Text = car.PurchasePrice.ToString();
            CmbModelYear.Text = car.ModelYear.ToString();
            CmbTypeCar.Text = car.Type.ToString();
            TxtMileage.Text = car.Mileage.ToString();
            string image = car.Type.ToString();
            if (image.Equals("Crossover"))
            {
                CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber1.jpg"));
            }
            else if (image.Equals("Sedan"))
            {
                CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber2.jpg"));
            }
            else if (image.Equals("Coupe"))
            {
                CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber3.jpg"));
            }
            else if (image.Equals("Hatchback"))
            {
                CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber4.jpg"));
            }
            else if (image.Equals("SUV"))
            {
                CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber5.jpg"));
            }
            else if (image.Equals("Convertible"))
            {
               CarImage.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CarImages/VinNumber6.jpg"));
            }

        }

        private void ClearUI() //method for clearing fields
        {
            TxtVinNumber.Text = "";
            TxtCarMake.Text = "";
            TxtPurchasePrice.Text = "";
            CmbModelYear.SelectedIndex = -1;
            CmbTypeCar.SelectedIndex = -1;
            TxtMileage.Text = "";
            TxtErrorMessage.Text = "";
            CarImage.Source = null;
            
        }

        private void OnAddCar(object sender, RoutedEventArgs e)
        {
            try
            {
                
                Car c = CaptureCarInformation();
                _carRepository.AddCar(c);
                LstCars.Items.Add(c); //add all items to list
            }
            catch (Exception ex)
            {
                TxtErrorMessage.Text = ex.Message;
            }
        }

        private void OnClear(object sender, RoutedEventArgs e)
        {
            ClearUI();
        }

        private void OnCarSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //1. get the selected car
            Car c = (Car)LstCars.SelectedItem;

            //2. render the fields in the textboxes
            RenderCarInfo(c);
            

        }
    }
}
